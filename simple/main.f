!    Welcome to this funny program
      program clion
        implicit none
        logical error
        integer m, n
        integer :: a = 1

        include 'include/type.f'

        type (simplex) :: s

        s%cardinality = 0

        call s1d(m, n)
        call subalg(s)

        if (writesimplex(s)) then
          write(*,*) 'Megaerror'
        endif

        contains
          subroutine subalg( s )
            type (simplex) :: s

            if (s%cardinality == 0) then
              s%cardinality = 1
            elseif (s%cardinality == 1) then
              s%cardinality = 1
            else
              write(*,*) 'Megaerror'
            endif
          endsubroutine

          function writesimplex(s)
            logical :: writesimplex
            type (simplex), intent(in) :: s
            write(*,*) 'Cardinality ' // 'is' , s%cardinality

            writesimplex = .TRUE.
          endfunction writesimplex

      end program

      subroutine s1d (a, b)
          integer a, b
!         Local variables
          integer tmp

          tmp = a
          a = b
          b = tmp

          return
      end
