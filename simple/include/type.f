        type :: simplex
                integer :: cardinality
                integer, dimension(2,4) :: witnessIDs
                real, dimension(3,4) :: coordinates
        end type simplex