!***********************************************************************************************************************************
!
!  Description:   This is the driver to call a function to compute the derminant of a matrix from:
!                   https://gist.github.com/yaremch/fe8fbc9f11c19d44dde291b858979796 
!  Author:        Mattia Montanari
!
!***********************************************************************************************************************************
      program fun_math_determinant
        implicit none
      
        integer ::  i, j, n
        DOUBLE PRECISION , DIMENSION(5,5) :: mat
        REAL :: d

        real :: numbers(5) !one dimensional integer array
        integer :: matrix(3,3)

        real :: AVRAGE
        DOUBLE PRECISION  :: FindDet

!-----------------------------------------------------------------------------------------------------------------------------------
        
        n = 5
        
        print *, 'Writing matrix..'
        
        do i = 1, n
          do j = 1, n
            mat(i,j) = 0.0
            mat(i,i) = i+j
          end do
        end do 
        mat(1,2)  = 1

        do i=1, n
          do j = 1, n
           ! print *, mat(i,j)
            write (*,'(E15.3)',advance="no") mat(i,j)
          end do
          write(*,*)
        end do
      
        PRINT*,"Determinant =", FindDet(mat,n)

        do i=1, n
          do j = 1, n
           ! print *, mat(i,j)
            write (*,'(E15.3)',advance="no") mat(i,j)
          end do
          write(*,*)
        end do
      end program


      DOUBLE PRECISION FUNCTION FindDet(matrix, n) 
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: n
      DOUBLE PRECISION , DIMENSION(n,n) :: matrix
      DOUBLE PRECISION :: m, temp, FindDetTemp
      INTEGER :: i, j, k, l
      LOGICAL :: DetExists = .TRUE.
      l = 1
      !Convert to upper triangular form
      DO k = 1, n-1
        IF (matrix(k,k) == 0) THEN
          DetExists = .FALSE.
          DO i = k+1, n
            IF (matrix(i,k) /= 0) THEN
              DO j = 1, n
                temp = matrix(i,j)
                matrix(i,j)= matrix(k,j)
                matrix(k,j) = temp
              END DO
              DetExists = .TRUE.
              l=-l
              EXIT
            ENDIF
          END DO
          IF (DetExists .EQV. .FALSE.) THEN
            FindDet = 0
            PRINT*,"  [ERROR in FindDet] Determinant Does not exist " 
            return
          END IF
        ENDIF
        DO j = k+1, n
          m = matrix(j,k)/matrix(k,k)
          DO i = k+1, n
            matrix(j,i) = matrix(j,i) - m*matrix(k,i)
          END DO
        END DO
      END DO
      
      !Calculate determinant by finding product of diagonal elements
      FindDetTemp = l
      DO i = 1, n
        FindDetTemp = FindDetTemp * matrix(i,i)
      END DO

      FindDet = FindDetTemp
      return
    END FUNCTION FindDet