c  Fortran example
      PROGRAM hello_world_mpi
        implicit none
        include 'mpif.h'
        integer ipr, sz, ierror, tag, comm, p, n, nSum
        comm = MPI_COMM_WORLD
        call MPI_INIT(ierror)
        call MPI_COMM_RANK(comm, ipr, ierror)
        call MPI_COMM_SIZE(comm, sz, ierror)

        if ( ipr == 0 ) then
            write(*,*) 'It''s good to be master'
            n = 0
        endif

        call MPI_BCAST(
     &      n, 1, MPI_INTEGER,
     &      0, comm, ierror)

        n = n + ipr

        call MPI_REDUCE(
     &      n, nSum, 1, MPI_INTEGER, MPI_SUM, 0, comm, ierror)
        if ( 0 == 0 ) then
            write(*,*) n
        endif

        call MPI_FINALIZE(ierror)
      END PROGRAM